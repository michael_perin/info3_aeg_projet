# Installation

## Récupération des sources depuis un dépôt Git

### Élisez un représentant de votre groupe qui effectuera la manip suivante 

- dans GitLab,
- dans le dépôt affecté à votre groupe,

1. créez un projet avec `Import project`
2. choississez `Repositiry by URL` et donnez l'url `https://gricad-gilab...` du dépôt git communiqué par l'enseignant
3. Comme `Project Name` donnez le nom `Parser_G<numéro du groupe>` (ex: Parser_G1)

### Ensuite, tous les membres du groupe  

1. ont accés à votre projet 
2. peuvent faire un `git clone` pour récupérer le contenu du dépôt

Ainsi vous partagez tous le dépôt avec le même point de départ.

## Importation dans Eclipse

Le projet est organisé en répe
- `workspace/` est le workspace pour Eclipse
- `JavaProject/` est le projet Java  

1. Lancez eclipse via la commande `/opt/eclipse/eclipse &` dans un terminal
2. Sélectionnez le répertoire `workspace` comme workspace
3. Utilisez la commande du menu File > open from et choississez le répertoire `JavaProject`
4. Depuis `JavaProject/src` ouvrez le menu `Build Path` > [Configure Build Path] 
    > JavaCC > Options : décochez STATIC ; [apply and close]
5. Onglet `Project` > `Clean`

## Exécution dans un Terminal

Dans le répertoire `test/`, tapez `make test`